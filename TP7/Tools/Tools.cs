﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tester.Tools {
    public class Tools {
        private Finder f;

        public Tools(Finder f) { this.f = f; }

        public object Instance(String objectName, object[] args) {
            Type t = f.get_class(objectName);
            Type[] args_type = new Type[args.Length];
            for (int i = 0; i < args.Length; ++i)
                args_type[i] = args[i].GetType();

            return f.get_ctor(t, args_type).Invoke(args);
        }

        public static Fact.Test.Result.Result file_equals(string reference, string compared) {

            if (!System.IO.File.Exists(reference) || !System.IO.File.Exists(compared))
                return new Fact.Test.Result.Error("File doesn't exist");

            byte[] b1 = System.IO.File.ReadAllBytes(reference);
            byte[] b2 = System.IO.File.ReadAllBytes(compared);

            if (b1.Length != b2.Length)
                return new Fact.Test.Result.Error("File doesn't have a correct size",
                    "Expected " + b1.Length + " bytes got " + b2.Length + " bytes");

            for (int i = 0; i < b1.Length; ++i)
                if (b1[i] != b2[i])
                    return new Fact.Test.Result.Error("File is different from the reference",
                        "difference happen at " + i + "th byte");

            return new Fact.Test.Result.Passed("File are identical");
        }

    }
}
