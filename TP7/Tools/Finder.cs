﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

using Fact.Processing;

using Function = System.Tuple<
    System.Type, System.Type, System.Reflection.MethodInfo>;
using System.Reflection.Emit;

namespace Tester.Tools {
    public class Finder {

        public Assembly a { get; private set; }
        public File f { get; private set; }

        public string _TempFolder { get; private set; }
        private string assembly_name;

        private Dictionary<string, List<Function>> methods = new Dictionary<string, List<Function>>();
        private Dictionary<string, Type> classes = new Dictionary<string, Type>();

        private Dictionary<string, Type> classes_memoized = new Dictionary<string, Type>();

        private void error(string error) {
            Fact.Assert.Misc.ExpectTestPass(new Fact.Test.Result.Error("Not Found", error));
        }

        public Finder(Project student, string exeHint = "") {
            string error_msg = "Could not find exe - Project did not compile or exe does not contain hint (" + exeHint + ")";
            if (student == null) error(error_msg);

            List<File> e = student.GetFiles("/", true, Fact.Processing.File.FileType.Executable);

            if (e == null) error(error_msg);

            List<File> exes = e.Where(
                x => !x.Name.Contains("vshost") && !x.Directory.Contains("obj")
             ).ToList();
            if (exes == null || exes.Count == 0) error(error_msg);

            List<String> exes_names = new List<string>();
            for (int i = 0; i < exes.Count; ++i)
                exes_names.Add(exes[i].Name.Substring(0, exes[i].Name.Length - 4));

            f = exes[Levenshtein.getClosestString(exeHint, exes_names)];

            if (f == null) error(error_msg);

            make_temp();
            assembly_name = _TempFolder + "\\" + f.Name;

            f.ExtractAt(_TempFolder);
            a = Assembly.Load(System.IO.File.ReadAllBytes(assembly_name));

            explore();
        }

        public void Cleanup() {
            Fact.Tools.RecursiveDelete(_TempFolder);
            Fact.Assert.IO.DirectoryNotExists(_TempFolder, Fact.Assert.Assert.Trigger.FATAL, "FileStreams Not Closed");
        }


        private void make_temp() {
            _TempFolder = Fact.Tools.CreateTempDirectory();
            Fact.Assert.IO.DirectoryExists(_TempFolder);
        }

        private void insert(Type namespace_, Type class_, string name, MethodInfo method) {
            if (!methods.ContainsKey(name))
                methods[name] = new List<Function>();
            methods[name].Add(new Function(namespace_, class_, method));
        }

        public MethodInfo get(string name) {
            if (!methods.ContainsKey(name))
                Fact.Assert.Misc.ExpectTestPass(new Fact.Test.Result.Error("Not Found", "Can't find Method " + name));

            if (methods[name].Count > 1)
                Fact.Assert.Misc.ExpectTestPass(new Fact.Test.Result.Error("Too many choices", "Too many methods mathing: " + name));
            return methods[name][0].Item3;
        }

        public Type get_class(string name)  {

            if (!this.classes_memoized.ContainsKey(name)) {
                if (!this.classes.ContainsKey(name))
                    this.classes_memoized.Add(name, this.classes[Levenshtein.getClosestString<Type>(name, classes)]);
                else
                    this.classes_memoized.Add(name, this.classes[name]);
            }

            return classes_memoized[name];
        }

        public ConstructorInfo get_ctor(Type class_, params Type[] arguments) {
            ConstructorInfo ctor = class_.GetConstructor(arguments);

            if (ctor == null) {
                string types = " {";
                for (int i = 0; i < arguments.Length; ++i)
                    if (i == arguments.Length - 1)
                        types += arguments[i];
                    else
                        types += arguments[i] + ", ";
                types += "}";

                Fact.Assert.Misc.ExpectTestPass(new Fact.Test.Result.Error("Not Found",
                    "Can't find Constructor with arguments " + types));
            }

            return ctor;
        }

        private void explore() {
            Type[] namespaces = a.GetTypes().Where(x => !x.FullName.Contains("PrivateImplementationDetails")).ToArray();
            foreach (Type n in namespaces) {
                Type[] classes = a.GetTypes().Where(
                    t => String.Equals(t.Namespace, n.Namespace, StringComparison.Ordinal)
                ).ToArray();

                foreach (Type c in classes) {
                    if (!this.classes.ContainsKey(c.Name)) {
                        this.classes.Add(c.Name, c);
                        foreach (var method in c.GetMethods(BindingFlags.Static | BindingFlags.NonPublic
                            | BindingFlags.Public | BindingFlags.InvokeMethod | BindingFlags.Default
                            | BindingFlags.Instance | BindingFlags.CreateInstance)) {
                            insert(n, c, method.Name, method);
                        }
                    }
                }
            }
        }

        private void listNamespaces() {
            foreach (Type t in a.GetTypes())
                if (!t.FullName.Contains("PrivateImplementationDetails"))
                    Console.WriteLine(t.FullName);

        }
    }
}