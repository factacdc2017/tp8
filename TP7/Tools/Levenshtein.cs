﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tester
{
    static class Levenshtein
    {
        private static int[,] cache;

        public static String getClosestString<T>(string wanted, Dictionary<String, T> elements) { 
            int min = Int32.MaxValue;
            String key = "";

            foreach (string k in elements.Keys) {
                int current = DistMemoisation(wanted, k);
                if (current < min) {
                    key = k;
                    min = current;
                }
            }
            return key;
        }

        public static int getClosestString(string wanted, List<String> elements) { 
            int min = Int32.MaxValue;
            int index = -1;

            for (int i = 0; i < elements.Count; ++i) {
                int current = DistMemoisation(wanted, elements[i]);
                if (current < min) {
                    index = i;
                    min = current;
                }
            }

            return index;
        }

        public static int DistMemoisation(string u, string v)
        {
            cache = new int[u.Length, v.Length];
            for (int i = 0; i < cache.GetLength(0); i++)
                for (int j = 0; j < cache.GetLength(1); j++)
                    cache[i, j] = -1;
            return DistCheck(u, v);
        }

        private static int DistCheck(string u, string v)
        {
            if (u.Length == 0 || v.Length == 0)
                return u.Length == 0 ? v.Length : u.Length;
            if (cache[u.Length - 1, v.Length - 1] == -1)
                cache[u.Length - 1, v.Length - 1] = Distance(u, v);
            return cache[u.Length - 1, v.Length - 1];
        }

        private static int Distance(string u, string v)
        {
            if (u[u.Length - 1] == v[v.Length - 1])
                return DistCheck(u.Substring(0, u.Length - 1), v.Substring(0, v.Length - 1));
            int d1 = DistCheck(u.Substring(0, u.Length - 1), v.Substring(0, v.Length - 1));
            int d2 = DistCheck(u, v.Substring(0, v.Length - 1));
            int d3 = DistCheck(u.Substring(0, u.Length - 1), v);
            return 1 + Math.Min(d1, Math.Min(d2, d3));
        }
    }
}
