﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tester.Tools {
    public class ConsoleCatcher {

        public string Out { get { return sw.GetStringBuilder().ToString(); } private set { this.Out = value; } }

        private TextWriter tmp;
        private StringWriter sw;

        public ConsoleCatcher() { }
        public ConsoleCatcher(bool acquire) { if (acquire) this.Acquire(); }

        public void Acquire() {
            tmp = Console.Out;
            sw = new StringWriter();
            Console.SetOut(sw);
        }

        public String Release() {
            Console.SetOut(tmp);
            return sw.GetStringBuilder().ToString();
        }
    }
}
