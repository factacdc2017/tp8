﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Fact;
using Tester.Tools;
using Tester;

using Tools = Tester.Tools;

namespace TP7
{
    [Fact.Attribute.Test.Group("Farm Validator")]
    public class FarmValidator {
        private Finder f;
        private Tools.Tools t;

        [Fact.Attribute.Test.Setup("Init")]
        public void Setup(Fact.Processing.Project student) {
            f = new Finder(student, "BackToBasics");
            t = new Tools.Tools(f);
        }


        [Fact.Attribute.Test.Test("Poney Validator")]
        public void poney_validator() {
            String reference = poney_ref(2);

            object instance = t.Instance("Pony", new object[] { 2, ConsoleColor.White });

            ConsoleCatcher c = new ConsoleCatcher(true);
            f.get("display").Invoke(obj: instance, parameters: new Object[] { });
            string result = c.Release();

            Fact.Assert.Basic.Equals(reference, result);
        }

        [Fact.Attribute.Test.Test("Farm Validator")]
        public void poney_validator() {
            String reference = "";

            object farmInstance = t.Instance("Farm", new object[] { "Poney Farm", 100 });
            int iterations = 5;

            for (int i = 0; i < iterations; ++i)
            {
                if (i != 0)
                    reference += " ";

                ConsoleCatcher c = new ConsoleCatcher(true);
                f.get("display").Invoke(obj: farmInstance, parameters: new Object[] { });
                string result = c.Release();

                Fact.Assert.Basic.Equals(reference, result);

                object poneyInstance = t.Instance("Pony", new object[] { 2, ConsoleColor.White });
                f.get("add_poney").Invoke(obj: farmInstance, parameters: new Object[] { poneyInstance });
                reference += poney_ref(2);
            }
        }


        private String poney_ref(int size) {
            String poney = ".";
            for (int i = 0; i < size; ++i)
                poney += "=";

            return poney + ".°";
        }
    }
}
